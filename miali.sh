#!/bin/bash

#sudo pacman -Syu yaourt
sudo pacman -Syu --noconfirm --needed xorg-server xorg-apps xf86-video-amdgpu lightdm openbox ttf-roboto ttf-ubuntu-font-family yaourt pulseaudio scrot bash-completion
sudo pacman -Syu --noconfirm --needed nemo gpicview gvfs-smb lxappearance-obconf nitrogen gsimplecal volumeicon lxsession tint2 # polkit-gnome numix-gtk-theme numix-frost-themes ttf-ubuntu-font-family
yaourt -Syu --noconfirm cpupower
yaourt -S --noconfirm paper-icon-theme-git
yaourt -S --noconfirm arc-gtk-theme
#yaourt -S cadence
#yaourt -S airwave-git
#yaourt -S winetricks
#winetricks vcrun2013
#winetricks vcrun2015
#winetricks mfc42

#mouseacc
mkdir -p /etc/X11/xorg.conf.d/
sudo tee /etc/X11/xorg.conf.d/50-mouse.conf >/dev/null << EOF
Section "InputClass"
	Identifier "Mouse"
	Driver "libinput"
	MatchIsPointer "yes"
	Option "AccelProfile" "flat"
EndSection
EOF

#redshift
mkdir -p ~/.config
tee ~/.config/redshift.conf >/dev/null << EOF
[redshift]
temp-night=3200
[manual]
lat=45.5
lon=-74
EOF

##swappiness####################################
mkdir -p /etc/sysctl.d/
if ! grep -q vm.swappiness /etc/sysctl.d/99-sysctl.conf; then
sudo tee -a /etc/sysctl.d/99-sysctl.conf >/dev/null << EOF
vm.swappiness=0
EOF
else
sed -i '/vm\.swappiness/ s/.*/vm.swappiness=0/' /etc/sysctl.d/99-sysctl.conf
fi

sudo sysctl --system

sudo gpasswd -a miali audio

#NETWORK SHARE
sudo mkdir -p /mnt/ubuntu
if ! grep -q 192.168.1.197 /etc/fstab; then
sudo tee -a /etc/fstab >/dev/null << 'EOF'
//192.168.1.197/ubuntu /mnt/ubuntu cifs users,nofail,x-systemd.automount,x-systemd.requires=network-online.target,rw,guest 0 0
EOF
fi
sudo mount -a

gsettings set org.cinnamon.desktop.default-applications.terminal exec 'termite'
#limit journal size
sudo sed -i '/SystemMaxUse/ s/.*/SystemMaxUse=10M/' /etc/systemd/journald.conf
sudo sed -i '/MAKEFLAGS=/s/^.*$/MAKEFLAGS=\"-j\$(nproc)\"/' /etc/makepkg.conf
sudo sed -e '/load-module module-suspend-on-idle/ s/^#*/#/' -i /etc/pulse/default.pa

#cpupower
sudo tee /etc/default/cpupower >/dev/null << EOF
governor='performance'
EOF

sudo tee /etc/lightdm/lightdm.conf << 'EOF'
[LightDM]
run-directory=/run/lightdm
#greeters-directory=$XDG_DATA_DIRS/lightdm/greeters:$XDG_DATA_DIRS/xgreeters

[Seat:*]
#greeter-session=
user-session=openbox
session-wrapper=/etc/lightdm/Xsession
autologin-user=pstn
autologin-user-timeout=0
#autologin-session=
EOF

systemctl --user enable redshift
sudo systemctl enable teamviewerd
sudo systemctl enable cpupower
sudo systemctl enable fstrim.timer
sudo systemctl enable lightdm

sudo grub-mkconfig -o /boot/grub/grub.cfg


