#!/bin/bash

if ! grep -q "\[multilib\]" /etc/pacman.conf; then
sudo tee -a /etc/pacman.conf >/dev/null << EOF
[multilib]
Include = /etc/pacman.d/mirrorlist
EOF
else
sudo sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
fi

sudo tee -a /etc/pacman.conf >/dev/null << 'EOF'
[archlinuxfr]
SigLevel = Never
Server = http://repo.archlinux.fr/$arch

EOF

sudo pacman -Syu --noconfirm --needed xorg-server xorg-apps lightdm openbox ttf-roboto ttf-ubuntu-font-family yaourt pulseaudio scrot bash-completion #xf86-video-amdgpu 
sudo pacman -Syu --noconfirm --needed gpicview gvfs-smb lxappearance-obconf nitrogen gsimplecal volumeicon lxsession tint2 # polkit-gnome numix-gtk-theme numix-frost-themes ttf-ubuntu-font-family
sudo pacman -Syu --noconfirm --needed remmina nemo termite firefox wine libvncserver steam mpv pavucontrol rhythmbox cpupower redshift steam-native-runtime # mediainfo-gui mkvtoolnix-gui
#sudo pacman -Syu --noconfirm --needed virtualbox-host-modules-arch virtualbox
#yaourt -S --noconfirm fsearch-git
yaourt -S --noconfirm paper-icon-theme-git
#yaourt -S --noconfirm arc-gtk-theme
yaourt -S --noconfirm pulseeffects calf-git
yaourt -S --noconfirm acestream-launcher
#yaourt -S --noconfirm plex-media-player
#yaourt -S --noconfirm makemkv
yaourt -S --noconfirm teamviewer

#archlabs
sudo tee -a /etc/pacman.conf >/dev/null << 'EOF'
[archlabs_repo]
Server = https://bitbucket.org/archlabslinux/archlabs_repo/raw/master/$arch
Server = https://sourceforge.net/project/archlabs-repo/files/archlabs_repo/$arch
EOF
#sudo pacman -Syu --noconfirm archlabs-themes
sudo sed '/\[archlabs_repo\]/ s/^#*/#/' -i /etc/pacman.conf
sudo pacman -Sy
#mouseacc
mkdir -p /etc/X11/xorg.conf.d/
sudo tee /etc/X11/xorg.conf.d/50-mouse.conf >/dev/null << EOF
Section "InputClass"
	Identifier "Mouse"
	Driver "libinput"
	MatchIsPointer "yes"
	Option "AccelProfile" "flat"
EndSection
EOF
#aliases
if ! grep -q acestream ~/.bashrc; then
tee -a ~/.bashrc >/dev/null << 'EOF'
ace () { 
acestream-launcher acestream://"$1"
}
EOF
fi
#redshift
mkdir -p ~/.config
tee ~/.config/redshift.conf >/dev/null << EOF
[redshift]
temp-night=3200
[manual]
lat=45.5
lon=-74
EOF
##swappiness####################################
mkdir -p /etc/sysctl.d/
if ! grep -q vm.swappiness /etc/sysctl.d/99-sysctl.conf; then
sudo tee -a /etc/sysctl.d/99-sysctl.conf >/dev/null << EOF
vm.swappiness=0
EOF
else
sed -i '/vm\.swappiness/ s/.*/vm.swappiness=0/' /etc/sysctl.d/99-sysctl.conf
fi

sudo sysctl --system

#NETWORK SHARE
sudo mkdir -p /mnt/ubuntu
if ! grep -q 192.168.1.197 /etc/fstab; then
sudo tee -a /etc/fstab >/dev/null << 'EOF'
LABEL=Storage /mnt/Storage auto nosuid,nodev,nofail,noatime 0 0
//192.168.1.197/ubuntu /mnt/ubuntu cifs guest,uid=pstn,comment=systemd.automount,nofail,rw,guest 0 0
EOF
fi
sudo mount -a


gsettings set org.cinnamon.desktop.default-applications.terminal exec 'termite'

#limit journal size
sudo sed -i '/SystemMaxUse/ s/.*/SystemMaxUse=10M/' /etc/systemd/journald.conf
sudo sed -i '/MAKEFLAGS=/s/^.*$/MAKEFLAGS=\"-j\$(nproc)\"/' /etc/makepkg.conf
sudo sed -e '/load-module module-suspend-on-idle/ s/^#*/#/' -i /etc/pulse/default.pa

#cpupower
sudo tee /etc/default/cpupower >/dev/null << EOF
governor='performance'
EOF

#################################
sudo tee /etc/lightdm/lightdm.conf << 'EOF'
[LightDM]
run-directory=/run/lightdm
#greeters-directory=$XDG_DATA_DIRS/lightdm/greeters:$XDG_DATA_DIRS/xgreeters

[Seat:*]
#greeter-session=
user-session=openbox
session-wrapper=/etc/lightdm/Xsession
autologin-user=pstn
autologin-user-timeout=0
#autologin-session=
EOF
##############
cp -RT /mnt/Storage/Files/scripts/config/.config/ ~/.config/
#shortcuts
#sudo cp /mnt/Storage/Files/shortcuts/desktop/*.desktop /usr/share/applications
#sudo cp /mnt/Storage/Files/scripts/copy/*.png /usr/share/icons

systemctl --user enable redshift
sudo systemctl enable teamviewerd
sudo systemctl enable cpupower
sudo systemctl enable fstrim.timer
sudo systemctl enable lightdm

sudo grub-mkconfig -o /boot/grub/grub.cfg

#mkdir -p ~/.ACEStream/.acestream_cache
#ln -s /mnt/ubuntu/6TB/.acestream_cache ~/.ACEStream/

#sudo systemctl enable systemd-networkd-wait-online
#remove network tray icon
#sudo mv /etc/xdg/autostart/nm-applet.desktop /etc/xdg/autostart/nm-applet.desktopbak
