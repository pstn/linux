#!/bin/bash

(
  flock -x -w 10 200 || exit 1

sleep 1





find "/mnt/Movies1/1" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb --conflict override --format "{plex}" --output "/mnt/Movies1" -r \;
find "/mnt/Movies2/2" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb --conflict override --format "{plex}" --output "/mnt/Movies2" -r \;
find "/mnt/Movies3/3" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb --conflict override --format "{plex}" --output "/mnt/Movies3" -r \;
find "/mnt/Movies3/fr" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb --conflict override --lang fr --format "/mnt/Movies3/Movies/{ny}/{ny}" -r \;



) 200>/var/lock/.myscript.exclusivelock
