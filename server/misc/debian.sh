#!/bin/bash

sudo sed -i '/cdrom/d' /etc/apt/sources.list
sudo apt-get -y update
sudo apt-get -y upgrade

# add repositories
sudo apt-get -y install curl apt-transport-https software-properties-common
echo "deb https://downloads.plex.tv/repo/deb ./public main" | sudo tee -a /etc/apt/sources.list
curl https://downloads.plex.tv/plex-keys/PlexSign.key | sudo apt-key add -
#wget -q -O - https://mkvtoolnix.download/gpg-pub-moritzbunkus.txt | sudo apt-key add -

sudo apt-get -y update

# install apps
sudo apt-get -y install firefox-esr nautilus gedit vlc mediainfo-gui mkvtoolnix mkvtoolnix-gui samba openjfx make gnome-disk-utility x11vnc
sudo apt-get -y install plexmediaserver
sudo apt-get -y install zlib1g-dev libboost-dev libboost-system-dev build-essential
sudo apt-get -y install libboost-chrono-dev libboost-random-dev libssl-dev libgeoip-dev
sudo apt-get -y install git pkg-config automake libtool
sudo apt-get -y install qtbase5-dev qttools5-dev-tools python

#install FileBot
cd
wget -O filebot.deb 'https://app.filebot.net/download.php?type=deb&arch=amd64'
sudo dpkg -i filebot.deb

#vnc script
cat >~/vnc.sh  <<EOL
#!/bin/bash
x11vnc -display :0 -forever -shared -o /home/debian/x11vnc.log -bg -rfbauth /home/debian/.vnc/passwd
EOL
chmod 777 vnc.sh

# install samba
sudo chmod 777 /etc/samba/smb.conf
cat >/etc/samba/smb.conf  <<EOL
[global]
workgroup = WORKGROUP
server string = Samba Server %v
netbios name = debian
security = user
map to guest = bad user
dns proxy = no
acl allow execute always = true

[debian]
   path = /mnt
   force user = debian
   force group = users
   create mask = 0644
   directory mask = 2755
   browsable = yes
   writable = yes
   guest ok = yes
EOL

sudo /etc/init.d/smbd restart
cd

# libtorrent
git clone https://github.com/arvidn/libtorrent.git
cd libtorrent
git checkout RC_1_0
./autotool.sh
./configure --disable-debug --enable-encryption --prefix=/usr
make clean && make
sudo make install
cd

# qBittorrent and empty the two files
wget https://sourceforge.net/projects/qbittorrent/files/qbittorrent/qbittorrent-3.3.16/qbittorrent-3.3.16.tar.xz/download -O qBitorrent.tar.xz
mkdir qBittorrent && tar xf qBitorrent.tar.xz -C qBittorrent --strip-components 1
cd qBittorrent
truncate -s 0 ./src/icons/skin/splash.png
truncate -s 0 ./src/icons/skin/mascot.png
./configure --prefix=/usr
make
sudo make install
cd

# plex agents
wget https://github.com/gboudreau/XBMCnfoMoviesImporter.bundle/archive/master.zip -O XBMCnfoMoviesImporter
wget https://github.com/gboudreau/XBMCnfoTVImporter.bundle/archive/master.zip -O XBMCnfoTVImporter
unzip XBMCnfoMoviesImporter
unzip XBMCnfoTVImporter
rm -rf XBMCnfoMoviesImporter
rm -rf XBMCnfoTVImporter
mv XBMCnfoMoviesImporter.bundle-master XBMCnfoMoviesImporter.bundle
mv XBMCnfoTVImporter.bundle-master XBMCnfoTVImporter.bundle
find . -name 'XBMC*' -exec sudo mv -t "/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Plug-ins" {} +
cd "/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Plug-ins"
sudo chown debian:1000 XBMC*
sudo service plexmediaserver restart

sudo mousepad /etc/lightdm/lightdm.conf
