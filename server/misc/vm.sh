#!/bin/bash

VERSION="0.9.14"

apt -y purge --autoremove pidgin*
apt -y purge --autoremove thunderbird
apt -y purge --autoremove transmission-*
apt -y purge --autoremove mines
apt -y purge --autoremove mines*
apt -y purge --autoremove sudoku*
apt -y purge --autoremove gnome-sudoku 

add-apt-repository ppa:certbot/certbot -y
apt -y update
apt -y install nginx libcairo2-dev libjpeg-turbo8-dev libpng12-dev libossp-uuid-dev libvncserver-dev libssl-dev libwebp-dev wget tomcat8 maven python-certbot-nginx x11vnc

#vnc
x11vnc -storepasswd
cat >~/vnc.sh  <<EOF
#!/bin/bash
x11vnc -display :0 -forever -shared -o ~/x11vnc.log -bg -rfbauth ~/.vnc/passwd
EOF
chmod 777 ~/vnc.sh

#ufw default deny
#ufw allow from 192.168.0.0/16 to any port 5900
#ufw allow 'Nginx HTTPS'
#ufw enable

wget http://apache.forsale.plus/guacamole/${VERSION}/source/guacamole-server-${VERSION}.tar.gz
tar -xzf guacamole-server-${VERSION}.tar.gz
cd guacamole-server-${VERSION}/
./configure --with-init-dir=/etc/init.d
make
make install
ldconfig
update-rc.d guacd defaults

cd /var/lib/tomcat8/webapps
wget http://apache.forsale.plus/guacamole/${VERSION}/binary/guacamole-${VERSION}.war -O guacamole.war
chmod 777 guacamole.war

mkdir /etc/guacamole
mkdir /usr/share/tomcat8/.guacamole
bash -c 'cat > /etc/guacamole/user-mapping.xml <<- EOF
<user-mapping>
	
    <!-- Per-user authentication and config information -->
    <authorize username="charles" password="">
        <protocol>vnc</protocol>
        <param name="hostname">localhost</param>
        <param name="port">5900</param>
        <param name="password"></param>
    </authorize>
</user-mapping>
EOF'

ln -s /etc/guacamole/user-mapping.xml /usr/share/tomcat8/.guacamole

/etc/init.d/tomcat8 start
/etc/init.d/guacd start

#revert root access
passwd -l root

rm /etc/nginx/sites-enabled/default
rm /etc/nginx/sites-available/default
cd /etc/nginx/sites-available/
wget https://raw.githubusercontent.com/pstnta/linux/master/pstn.pw
service guacd start
ln -s /etc/nginx/sites-available/pstn.pw /etc/nginx/sites-enabled/pstn.pw
#certbot --nginx -d pstn.pw -d www.pstn.pw
