#!/bin/bash

sudo apt-get update
sudo apt-get -y install curl apt-transport-https
sudo apt-get -y install libboost-dev libboost-system-dev build-essential
sudo apt-get -y install libboost-chrono-dev libboost-random-dev libssl-dev libgeoip-dev
sudo apt-get -y install git pkg-config automake libtool
sudo apt-get -y install qtbase5-dev qttools5-dev-tools python

# libtorrent
git clone https://github.com/arvidn/libtorrent.git
cd libtorrent
git checkout RC_1_0
./autotool.sh
./configure --disable-debug --enable-encryption --prefix=/usr
make clean && make
make install
cd

# qBittorrent and empty the two files
wget https://sourceforge.net/projects/qbittorrent/files/qbittorrent/qbittorrent-3.3.15/qbittorrent-3.3.15.tar.xz/download -O qBitorrent.tar.xz
mkdir qBittorrent && tar xf qBitorrent.tar.xz -C qBittorrent --strip-components 1
cd qBittorrent
truncate -s 0 ./src/icons/skin/splash.png
truncate -s 0 ./src/icons/skin/mascot.png
./configure --prefix=/usr
make
make install
