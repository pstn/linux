#!/bin/bash

pacstrap /mnt base base-devel
genfstab -U /mnt >> /mnt/etc/fstab
arch-chroot /mnt
exit #not sure
ln -sf /usr/share/zoneinfo/America/Toronto /etc/localtime
hwclock --systohc
echo 'en_CA.UTF-8 UTF-8' > /etc/locale.gen
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
echo archlinux > /etc/hostname
useradd -m pstn
passwd pstn
groupadd -r autologin
gpasswd -a pstn autologin
#
tee /etc/hosts << 'EOF'
127.0.0.1	localhost
::1		localhost
127.0.1.1	archlinux.localdomain	archlinux
EOF
#
tee -a /etc/sudoers << 'EOF'
pstn ALL=(ALL) ALL
EOF
#
systemctl enable dhcpcd
pacman -S grub os-prober
grub-install /dev/sda
grub-mkconfig -o /boot/grub/grub.cfg
passwd

#/etc/lightdm/lightdm.conf
#user-session=openbox
#session-wrapper=/etc/lightdm/Xsession
#autologin-user=pstn
#autologin-user-timeout=0

pacman -Syu --noconfirm --needed xorg openbox lightdm yaourt

#systemctl enable dhcpcd@enp0s3.service
#systemctl restart systemd-networkd
#systemctl enable systemd-networkd

#cp -R /etc/xdg/openbox ~/.config/
