#!/bin/bash

sudo apt-get -y update
sudo apt-get -y upgrade
#sudo apt-get -y install xfce4

# add repositories
sudo apt-get -y install curl apt-transport-https software-properties-common
echo "deb https://downloads.plex.tv/repo/deb ./public main" | sudo tee -a /etc/apt/sources.list
curl https://downloads.plex.tv/plex-keys/PlexSign.key | sudo apt-key add -
sudo apt-get -y update

# apps
sudo apt-get -y install firefox nautilus fail2ban mousepad gvfs-backends gnome-disk-utility vlc mediainfo-gui mkvtoolnix mkvtoolnix-gui x11vnc samba openjfx make git gettext
sudo apt-get -y install plexmediaserver
# qbit tools
sudo apt-get -y install build-essential pkg-config automake libtool git
sudo apt-get -y install libboost-dev libboost-system-dev libboost-chrono-dev libboost-random-dev libssl-dev
sudo apt-get -y install qtbase5-dev qttools5-dev-tools libqt5svg5-dev
sudo apt-get -y install python3

#vnc
x11vnc -storepasswd
cat >>~/vnc.sh  <<EOL
#!/bin/bash
x11vnc -display :0 -forever -shared -o ~/x11vnc.log -bg -rfbauth ~/.vnc/passwd
EOL
sudo chmod 777 ~/vnc.sh

#install FileBot
cd
wget -O filebot.deb 'https://app.filebot.net/download.php?type=deb&arch=amd64'
sudo dpkg -i filebot.deb

# install samba
sudo chmod 777 /etc/samba/smb.conf
cat >/etc/samba/smb.conf  <<EOL
[global]
workgroup = WORKGROUP
server string = Samba Server %v
netbios name = home
security = user
map to guest = bad user
dns proxy = no
acl allow execute always = true

[server]
   path = /mnt
   force user = home
   force group = users
   create mask = 0644
   directory mask = 2755
   browsable = yes
   writable = yes
   guest ok = yes
EOL

sudo /etc/init.d/smbd restart

cd

# libtorrent
git clone https://github.com/arvidn/libtorrent.git
cd libtorrent
git checkout RC_1_0
./autotool.sh
./configure --disable-debug --enable-encryption CXXFLAGS=-std=c++11
make clean && make -j2
sudo make install
cd

# qBittorrent and empty the two files
wget https://sourceforge.net/projects/qbittorrent/files/qbittorrent/qbittorrent-4.0.1/qbittorrent-4.0.1.tar.xz/download -O qBitorrent.tar.xz
mkdir qBittorrent && tar xf qBitorrent.tar.xz -C qBittorrent --strip-components 1
cd qBittorrent
truncate -s 0 ./src/icons/skin/splash.png
truncate -s 0 ./src/icons/skin/mascot.png
./configure
make -j2
sudo make install

cd
# plex agents
wget https://github.com/gboudreau/XBMCnfoMoviesImporter.bundle/archive/master.zip -O XBMCnfoMoviesImporter
wget https://github.com/gboudreau/XBMCnfoTVImporter.bundle/archive/master.zip -O XBMCnfoTVImporter
unzip XBMCnfoMoviesImporter
unzip XBMCnfoTVImporter
rm -rf XBMCnfoMoviesImporter
rm -rf XBMCnfoTVImporter
mv XBMCnfoMoviesImporter.bundle-master XBMCnfoMoviesImporter.bundle
mv XBMCnfoTVImporter.bundle-master XBMCnfoTVImporter.bundle
find . -name 'XBMC*' -exec sudo mv -t "/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Plug-ins" {} +
cd "/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Plug-ins"
sudo chown home:1000 XBMC*
sudo service plexmediaserver restart
