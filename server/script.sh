#!/bin/bash

(
  flock -x -w 10 200 || exit 1

sleep 1

source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/1 /mnt/6TB/Downloads/movies/movies1
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/2 /mnt/6TB/Downloads/movies/movies2
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/3 /mnt/6TB/Downloads/movies/movies3
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/4 /mnt/6TB/Downloads/movies/movies4
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/fr /mnt/6TB/Downloads/movies/fr
#source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/ozane /mnt/6TB/Downloads/ozane
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/series/completed /mnt/6TB/Downloads/series/series
find "/mnt/6TB/Downloads/movies/movies1" -iname "*.mkv" -size +550 -execdir mv {} "/mnt/6TB/Downloads/movies/completed/1/" \;
find "/mnt/6TB/Downloads/movies/movies2" -iname "*.mkv" -size +550 -execdir mv {} "/mnt/6TB/Downloads/movies/completed/2/" \;
find "/mnt/6TB/Downloads/movies/movies3" -iname "*.mkv" -size +550 -execdir mv {} "/mnt/6TB/Downloads/movies/completed/3/" \;
find "/mnt/6TB/Downloads/movies/movies4" -iname "*.mkv" -size +550 -execdir mv {} "/mnt/6TB/Downloads/movies/completed/4/" \;
find "/mnt/6TB/Downloads/movies/fr" -iname "*.mkv" -size +550 -execdir mv {} "/mnt/6TB/Downloads/movies/completed/fr/" \;
find "/mnt/6TB/Downloads/series/series" -iname "*.mkv" -size +550 -execdir mv {} "/mnt/6TB/Downloads/series/completed/" \;
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/series/completed /mnt/6TB/Downloads/series/completed
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/1 /mnt/6TB/Downloads/movies/completed/1
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/2 /mnt/6TB/Downloads/movies/completed/2
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/3 /mnt/6TB/Downloads/movies/completed/3
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/4 /mnt/6TB/Downloads/movies/completed/4
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/movies/completed/fr /mnt/6TB/Downloads/movies/completed/fr
source /home/ubuntu/unrar.sh --clean=all --output /mnt/6TB/Downloads/extract /mnt/6TB/Downloads/extract

sleep 11

find /mnt/6TB/Downloads/movies/movies1 -type f \( -iname "*.nfo" -o -iname "*.sfv" -o -iname "*.jpg" -o -iname "*.png" -o -iname "*.txt" -o -iname "*sample*" -o -iname "*.md5" -o -iname "*.mov" \) -size -550M -delete
find /mnt/6TB/Downloads/movies/movies2 -type f \( -iname "*.nfo" -o -iname "*.sfv" -o -iname "*.jpg" -o -iname "*.png" -o -iname "*.txt" -o -iname "*sample*" -o -iname "*.md5" -o -iname "*.mov" \) -size -550M -delete
find /mnt/6TB/Downloads/movies/movies3 -type f \( -iname "*.nfo" -o -iname "*.sfv" -o -iname "*.jpg" -o -iname "*.png" -o -iname "*.txt" -o -iname "*sample*" -o -iname "*.md5" -o -iname "*.mov" \) -size -550M -delete
find /mnt/6TB/Downloads/movies/fr -type f \( -iname "*.nfo" -o -iname "*.sfv" -o -iname "*.jpg" -o -iname "*.png" -o -iname "*.txt" -o -iname "*sample*" -o -iname "*.md5" -o -iname "*.mov" \) -size -550M -delete
find /mnt/6TB/Downloads -type f -iname "*sample*" -size -550M -delete

find "/mnt/6TB/Downloads/movies/completed/1" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb --conflict override --format "/mnt/Movies1/Movies/{ny}/{ny}" -r \;
find "/mnt/6TB/Downloads/movies/completed/2" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb --conflict override --format "/mnt/Movies2/Movies/{ny}/{ny}" -r \;
find "/mnt/6TB/Downloads/movies/completed/3" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb -non-strict --conflict override --format "/mnt/Movies3/Movies/{ny}/{ny}" -r \;
find "/mnt/6TB/Downloads/movies/completed/4" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb -non-strict --conflict override --format "/mnt/Movies4/Movies/{ny}/{ny}" -r \;
find "/mnt/6TB/Downloads/movies/completed/fr" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb -non-strict --conflict override --lang fr --format "/mnt/Movies3/Movies/{ny}/{ny}" -r \;
find "/mnt/6TB/Downloads/movies/ozane" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -execdir filebot -rename {} --db themoviedb -non-strict --conflict override --format "/mnt/OzDan/movies.ozane/{ny}/{ny}" -r \;
#find "/mnt/6TB/Downloads/movies" -mmin +0.1 \( -iname '*.mkv' -o -iname '*.mp4' -o -iname '*.avi' \) -type f -not -path "*movies1*" -not -path "*movies2*" -not -path "*movies3*" -not -path "*movies4*" -execdir filebot -rename {} --db themoviedb --conflict skip --action copy --conflict skip --format "/mnt/Movies4/Movies/{ny}/{ny}" -r \;


find /mnt/6TB/Downloads/movies/movies1 -mindepth 1 -type d -exec du -ks {} + | awk '$1 <= 150000' | cut -f 2- | xargs -d \\n rm -rf
find /mnt/6TB/Downloads/movies/movies2 -mindepth 1 -type d -exec du -ks {} + | awk '$1 <= 150000' | cut -f 2- | xargs -d \\n rm -rf
find /mnt/6TB/Downloads/movies/movies3 -mindepth 1 -type d -exec du -ks {} + | awk '$1 <= 150000' | cut -f 2- | xargs -d \\n rm -rf
find /mnt/6TB/Downloads/movies/movies4 -mindepth 1 -type d -exec du -ks {} + | awk '$1 <= 150000' | cut -f 2- | xargs -d \\n rm -rf
find /mnt/6TB/Downloads/movies/fr -mindepth 1 -type d -exec du -ks {} + | awk '$1 <= 150000' | cut -f 2- | xargs -d \\n rm -rf



find /mnt/6TB/Downloads/series/completed -type f -iname "The.Walking.Dead.S0*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series2/Series/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" -r \;
find /mnt/6TB/Downloads/series/completed -type f -iname "American.Crime.Story.S0*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series2/Series/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" -r \;
find /mnt/6TB/Downloads/series/completed -type f -iname "Homeland.S0*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series1/Series/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" -r \;
find /mnt/6TB/Downloads/series/completed -type f -iname "Power*S0*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series2/Series/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" -r \;
find /mnt/6TB/Downloads/series/completed -type f -iname "Better.Call.Saul.S0*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series2/Series/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" -r \;
find /mnt/6TB/Downloads/series/completed -type f -iname "Jane*.S0*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/OzDan/series.ozane/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" \;
find /mnt/6TB/Downloads/series/completed -type f -iname "The.Americans*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series2/Series/The Americans/{'Season '+s.pad(2)}/The Americans - {s00e00} - {t}" -r \;
#find /mnt/6TB/Downloads/series/completed -type f -iname "Mr.Mercedes*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series3/Series2/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" -r \;
find /mnt/6TB/Downloads/series/completed -type f -iname "American*Dad*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/OzDan/series.ozane/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" \;
find /mnt/6TB/Downloads/series/completed -type f -iname "Shameless*S08*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series2/Series/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" -r \;
#find /mnt/6TB/Downloads/series/completed -type f -iname "Fargo.S0*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series2/Series/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" -r \;
#find /mnt/6TB/Downloads/extract -type f -iname "*S0*E*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/Series3/Series2/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" -r \;
find /mnt/6TB/Downloads/series/ozdan -type f -iname "*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict --format "/mnt/OzDan/Series/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" \;
find /mnt/6TB/Downloads/series/completed -type f -iname "*.mkv" -exec filebot -rename {} --db TheTVDB -non-strict  --action copy --format "/mnt/Series2/Series/{n}/{'Season '+s.pad(2)}/{n} - {s00e00} - {t}" \;

) 200>/var/lock/.myscript.exclusivelock
