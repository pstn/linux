#!/bin/bash

screen1=DisplayPort-1
screen2=HDMI-A-1

xrandr --output $screen1 --primary --mode 1920x1080 --rate 144 --scale 1x1
xrandr --output $screen2 --off
