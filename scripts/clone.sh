#!/bin/bash

if xrandr | grep -q DP-
then
screen1=DP-2
screen2=HDMI-2
elif xrandr | grep -q DisplayPort-
then
screen1=DisplayPort-1
screen2=HDMI-A-1
fi

xrandr --output $screen2 --primary --auto --output $screen1 --same-as $screen2 --mode 1920x1080
