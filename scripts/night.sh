#!/bin/bash

if systemctl --quiet --user is-active redshift ; then
	systemctl --user stop redshift
else
	systemctl --user start redshift
fi