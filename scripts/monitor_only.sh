#!/bin/bash

if xrandr | grep -q DP-
then
screen1=DP-2
screen2=HDMI-2
elif xrandr | grep -q DisplayPort-
then
screen1=DisplayPort-1
screen2=HDMI-A-1
fi

#export TIMEOUT=600


function waitformonitor () {
until xrandr | grep -m 1 "144"; do sleep 1 ; done
}
export -f waitformonitor
timeout 1800s bash -c waitformonitor

if [[ $? = 124 ]]; then exit $?; fi

#xrandr --output $screen1 --primary --auto
xrandr --output $screen1 --primary  --mode 1920x1080 --rate 144
xrandr --output $screen2 --off
nitrogen --restore