#!/bin/bash


#apps
sudo pacman -Syu --noconfirm mpv rhythmbox soundconverter
yaourt -S --noconfirm scribus
yaourt -S --noconfirm gimp
yaourt -S --noconfirm kodi
yaourt -S --noconfirm skypeforlinux-bin
#yaourt -S --noconfirm teamviewer
yaourt -S --noconfirm tiny-media-manager
yaourt -S --noconfirm filebot
yaourt -S --noconfirm youtube-dl-gui-git
yaourt -S --noconfirm paper-icon-theme-git
yaourt -S --noconfirm arc-gtk-theme
yaourt -S --noconfirm wire-desktop

sudo gpasswd -a pulse audio

sudo cpupower frequency-set -g performance

sudo systemctl enable fstrim.timer

#mouseacc
sudo tee /etc/X11/xorg.conf.d/50-mouse-acceleration.conf &>/dev/null << EOF
Section "InputClass"
	Identifier "My Mouse"
	Driver "libinput"
	MatchIsPointer "yes"
	Option "AccelProfile" "flat"
EndSection
EOF


#wget https://download.teamviewer.com/download/version_11x/teamviewer_qs.tar.gz -O teamviewer.tar.xz
#tar xf teamviewer.tar.xz

echo ubuntu | sudo -S systemctl enable teamviewerd.service
echo ubuntu | sudo -S systemctl start teamviewerd.service

#titlebar
cat >~/.config/gtk-3.0/gtk.css  <<EOL
window.ssd headerbar.titlebar {
    padding-top: 0px;
    padding-bottom: 0px;
    min-height: 0;
}

window.ssd headerbar.titlebar button.titlebutton {
    padding: 0px;
    min-height: 0;
    min-width: 0;
}
EOL

gsettings set org.gnome.nautilus.icon-view default-zoom-level 'small'


#install gnome extensions
sudo rm -rf /usr/share/gnome-shell/extensions
wget -O steal-my-focus@kagesenshi.org.zip "https://github.com/tak0kada/gnome-shell-extension-stealmyfocus/archive/master.zip"
unzip steal-my-focus@kagesenshi.org.zip
mv gnome-shell-extension-stealmyfocus-master ~/.local/share/gnome-shell/extensions/steal-my-focus@kagesenshi.org
rm ~/.local/share/gnome-shell/extensions/steal-my-focus@kagesenshi.org/Makefile
rm ~/.local/share/gnome-shell/extensions/steal-my-focus@kagesenshi.org/README.md
wget -O gnome-shell-extension-installer "https://github.com/brunelli/gnome-shell-extension-installer/raw/master/gnome-shell-extension-installer"
sudo chmod +x gnome-shell-extension-installer
sudo mv gnome-shell-extension-installer /usr/bin/
gnome-shell-extension-installer 1160 19 118 --restart-shell

teamviewer

